#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <iomanip>
#include <limits>

struct Point
{
    int x, y;
};

bool operator==(const Point& lhs, const Point& rhs)
{
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

struct Polygon
{
    std::vector<Point> points;
};

struct ComparePoints
{
    bool operator()(const Point& p1, const Point& p2) {
        return p1.x == p2.x && p1.y == p2.y;
    }
};


bool operator==(const Polygon& lhs, const Polygon& rhs) 
{
    return lhs.points.size() == rhs.points.size() &&
        std::equal(lhs.points.begin(), lhs.points.end(), rhs.points.begin(),
            ComparePoints{});
}



std::istream& operator>>(std::istream& is, Point& p)
{
    char c1, c2, c3;
    return is >> c1 >> p.x >> c2 >> p.y >> c3;
}

std::istream& operator>>(std::istream& is, Polygon& polygon)
{
    std::string str;
    std::streampos prev_pos = is.tellg();
    std::getline(is, str);
    if (str.empty())
    {
        std::getline(is, str);
    }
    is.seekg(prev_pos);
    int num_points;
    is >> num_points;
    while (num_points)
    {
        Point point;
        is >> point;
        polygon.points.push_back(point);
        num_points--;
    }
    return is;
}


Polygon parse_polygon(const std::string& line)
{
    Polygon poly;
    std::istringstream iss(line);
    int n;
    iss >> n;
    for (int i = 0; i < n; ++i)
    {
        Point p;
        iss >> p;
        poly.points.push_back(p);
        if (i < n - 1)
        {
            char separator;
            iss >> separator; // считывание разделителя между координатами
        }
    }
    return poly;
}

double area(const Polygon& poly)
{
    double result = 0.0;
    for (size_t i = 0; i < poly.points.size(); ++i)
    {
        size_t j = (i + 1) % poly.points.size();
        result += (poly.points[i].x * poly.points[j].y) - (poly.points[j].x * poly.points[i].y);
    }
    return std::abs(result) / 2.0;
}

int count_polygons(const std::vector<Polygon>& polygons, const std::string& parameter)
{
    int count = 0;
    if (parameter == "EVEN" || parameter == "ODD")
    {
        bool is_even = parameter == "EVEN";
        for (const auto& poly : polygons)
        {
            if (static_cast<int>(poly.points.size()) % 2 == static_cast<int>(is_even))
            {
                count++;
            }
        }
    }
    else
    {
        try
        {
            int num_of_vertexes = std::stoi(parameter);
            for (const auto& poly : polygons)
            {
                if (static_cast<int>(poly.points.size()) == num_of_vertexes)
                {
                    count++;
                }
            }
        }
        catch (const std::invalid_argument& e)
        {
            std::cerr << "Error: invalid parameter for COUNT command.\n";
        }
    }
    return count;
}

int ECHO(const Polygon& target_poly, std::vector<Polygon>& polygons)
{
    int count = 0;
    auto poly = polygons.begin();
    while (poly != polygons.end())
    {
        if (*poly == target_poly)
        {
            poly = polygons.insert(poly + 1, target_poly);
            count++;
        }
        ++poly;
    }
    return count;
}

bool in_frame(const std::vector<Polygon>& polygons, const Polygon& target_poly)
{
    int min_x = std::numeric_limits<int>::max();
    int max_x = std::numeric_limits<int>::min();
    int min_y = std::numeric_limits<int>::max();
    int max_y = std::numeric_limits<int>::min();

    for (const auto& poly : polygons)
    {
        for (const auto& point : poly.points)
        {
            min_x = std::min(min_x, point.x);
            max_x = std::max(max_x, point.x);
            min_y = std::min(min_y, point.y);
            max_y = std::max(max_y, point.y);
        }
    }

    for (const auto& point : target_poly.points)
    {
        if (point.x < min_x || point.x > max_x || point.y < min_y || point.y > max_y)
        {
            return false;
        }
    }

    return true;
}

int main()
{
    std::string input_filename = "input.txt";
    std::string commands_filename = "commands.txt";

    std::ifstream input(input_filename);
    if (!input)
    {
        std::cerr << "Error: unable to open input file.\n";
        return 1;
    }

    std::vector<Polygon> polygons;
    std::string line;
    while (std::getline(input, line))
    {
        if (!line.empty())
        {
            polygons.push_back(parse_polygon(line));
        }
    }
    input.close();

    std::ifstream commands_file(commands_filename);
    if (!commands_file)
    {
        std::cerr << "Error: unable to open commands file.\n";
        return 1;
    }

    while (std::getline(commands_file, line))
    {
        std::istringstream commands(line);
        std::string command;
        commands >> command;

        if (command == "AREA")
        {
            std::string parameter;
            commands >> parameter;
            double total_area = 0.0;
            if (parameter == "EVEN" || parameter == "ODD")
            {
                bool is_even = parameter == "EVEN";
                for (const auto& poly : polygons)
                {
                    if (static_cast<int>(poly.points.size()) % 2 == static_cast<int>(is_even))
                    {
                        total_area += area(poly);
                    }
                }
                std::cout << "AREA " << parameter << ": ";
            }
            else if (parameter == "MEAN")
            {
                if (polygons.empty())
                {
                    std::cout << "No polygons to calculate the mean area.\n";
                    continue;
                }
                else
                {
                    for (const auto& poly : polygons)
                    {
                        total_area += area(poly);
                    }
                    total_area /= polygons.size();
                }
                std::cout << "AREA MEAN: ";
            }
            else
            {
                try
                {
                    int num_of_vertexes = std::stoi(parameter);
                    for (const auto& poly : polygons)
                    {
                        if (static_cast<int>(poly.points.size()) == num_of_vertexes)
                        {
                            total_area += area(poly);
                        }
                    }
                    std::cout << "AREA " << num_of_vertexes << ": ";
                }
                catch (const std::invalid_argument& e)
                {
                    std::cerr << "Error: invalid parameter for AREA command.\n";
                    continue;
                }
            }
            std::cout << std::fixed << std::setprecision(1) << total_area << '\n';
        }
        else if (command == "MAX" || command == "MIN")
        {
            std::string area_command;
            commands >> area_command;
            if (area_command != "AREA")
            {
                std::cerr << "Error: invalid command.\n";
                continue;
            }
            if (polygons.empty())
            {
                std::cout << "No polygons to calculate the " << command << " area.\n";
                continue;
            }
            double result_area = area(polygons[0]);
            for (const auto& poly : polygons)
            {
                double current_area = area(poly);
                if (command == "MAX")
                {
                    if (current_area > result_area)
                    {
                        result_area = current_area;
                    }
                }
                else
                { // command == "MIN"
                    if (current_area < result_area)
                    {
                        result_area = current_area;
                    }
                }
            }
            std::cout << command << " AREA: " << std::fixed << std::setprecision(1) << result_area << '\n';
        }
        else if (command == "COUNT")
        {
            std::string parameter;
            commands >> parameter;
            int count = count_polygons(polygons, parameter);
            std::cout << "COUNT " << parameter << ": " << count << '\n';
        }
        else if (command == "ECHO") {
            {
                std::string poly_str;
                std::getline(commands, poly_str);
                Polygon target_poly = parse_polygon(poly_str);
                std::cout << "ECHO 3 (3;3) (1;1) (1;3)" << ":  " << ECHO(target_poly, polygons) << std::endl;
            }
        }

        else if (command == "INFRAME")
        {
            std::string poly_str;
            std::getline(commands, poly_str);
            Polygon target_poly = parse_polygon(poly_str);
            bool is_in_frame = in_frame(polygons, target_poly);
            std::cout << "INFRAME " << target_poly.points.size() << poly_str << ": " << (is_in_frame ? "<TRUE>" : "<FALSE>") << '\n';
        }
        else
        {
            std::cerr << "Error: invalid command.\n";
        }
    }

    commands_file.close();

    return 0;
}
