#ifndef AVLTREE_H
#define AVLTREE_H

#include <iostream>
#include <map>
#include <string>
#include <fstream>

class Dictionary {
private:
    std::map<std::string, std::string> avlTree;

public:
    void addTranslation(const std::string& englishWord, const std::string& russianWord);
    std::string getTranslation(const std::string& englishWord);
    void printAllTranslations();
    void removeTranslation(const std::string& englishWord);
    void addWordsFromFile(const std::string& filename);
};


void Dictionary::addTranslation(const std::string& englishWord, const std::string& russianWord) {
    avlTree[englishWord] = russianWord;
}


std::string Dictionary::getTranslation(const std::string& englishWord) {
    auto it = avlTree.find(englishWord);
    if (it != avlTree.end()) {
        return it->second;
    }
    else {
        return "������� �� ������";
    }
}

 
void Dictionary::printAllTranslations() {
    for (const auto& entry : avlTree) {
        std::cout << entry.first << " -> " << entry.second << std::endl;
    }
}

 
void Dictionary::removeTranslation(const std::string& englishWord) {
    auto it = avlTree.find(englishWord);
    if (it != avlTree.end()) {
        avlTree.erase(it);
        std::cout << "������� ������" << std::endl;
    }
    else {
        std::cout << "������� �� ������" << std::endl;
    }
}

void Dictionary::addWordsFromFile(const std::string& filename) {
    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cerr << "�� ������� ������� ����: " << filename << std::endl;
        return;
    }

    std::string englishWord, russianWord;
    while (file >> englishWord >> russianWord) {
        addTranslation(englishWord, russianWord);
    }

    file.close();
    std::cout << "����� ������� ���������" << std::endl;
}

#endif
