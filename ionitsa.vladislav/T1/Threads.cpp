﻿#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <sstream>

struct DataStruct
{
    unsigned long long key1;
    long long key2;
    std::string key3;
};

std::istream& operator>>(std::istream& is, DataStruct& data)
{
    char c;
    is >> c; 

    while (is >> c && c != ')')
    {
        std::string key_name;
        is >> key_name >> std::ws; 

        if (key_name == "key1")
        {
            is >> std::hex >> data.key1; 
        }
        else if (key_name == "key2")
        {
            is >> data.key2; 
        }
        else if (key_name == "key3")
        {
            std::getline(is, data.key3, ':');
        }
        else
        {
            std::string unused;
            std::getline(is, unused);
            break;
        }

        is >> c;
    }

    return is;
}

std::ostream& operator<<(std::ostream& os, const DataStruct& data)
{
    os << "(:key1 " << std::hex << data.key1 << ":"
        << "key2 " << data.key2 << ":"
        << "key3 \"" << data.key3 << "\":)";

    return os;
}

bool CompareDataStruct(const DataStruct& lhs, const DataStruct& rhs)
{
    if (lhs.key1 != rhs.key1)
    {
        return lhs.key1 < rhs.key1; 
    }
    else if (lhs.key2 != rhs.key2)
    {
        return lhs.key2 < rhs.key2; 
    }
    else if (lhs.key3.length() != rhs.key3.length())
    {
        return lhs.key3.length() < rhs.key3.length(); 
    }
    else
    {
        return lhs.key3 < rhs.key3; 
    }
}

int main()
{
    std::istringstream iss("(:key1 10ull:key2 'c':key3 \"Data\":)\n(:key3 \"Data\":key2 'c':key1 10ull:)\n(:key2 -89LL:key3 \"Data\":key1 89ll:)");

    std::vector<DataStruct> data_vector;
    std::copy(std::istream_iterator<DataStruct>(iss >> std::ws), std::istream_iterator<DataStruct>(), std::back_inserter(data_vector));

    std::sort(data_vector.begin(), data_vector.end(), CompareDataStruct);

    std::ostream_iterator<DataStruct> out_it(std::cout, "\n");
    std::copy(data_vector.begin(), data_vector.end(), out_it);

    return 0;
}