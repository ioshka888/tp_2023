#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <sstream>
#include <iterator>

struct DataStruct
{
    unsigned long long key1;
    double key2;
    std::string key3;
    bool valid = false;
};

std::istream& operator>>(std::istream& iss, DataStruct& data)
{
    char c;
    while (iss >> c)
    {
        if (c == ':')
        {
            std::string key;
            iss >> key;
            if (key == "key2")
            {
                double value;
                if (iss >> value)
                {
                    data.key2 = value;
                }
                else
                {
                    data.valid = false;
                    return iss;
                }
            }
            else if (key == "key1")
            {
                unsigned long long value;
                if (iss >> value)
                {
                    data.key1 = value;
                }
                else
                {
                    data.valid = false;
                    return iss;
                }
            }
            else if (key == "key3")
            {
                std::string value;
                if (iss >> c && c == '"' && getline(iss, value, '"'))
                {
                    data.key3 = value;
                }
                else
                {
                    data.valid = false;
                    return iss;
                }
            }
            else
            {
                break;
            }
        }
        else if (c == ')')
        {
            data.valid = true;
            break;
        }
    }
    return iss;
}

std::string double_to_literal_string(double value)
{
    std::ostringstream ss;
    ss << std::fixed << std::setprecision(1) << value << "d";
    return ss.str();
}

std::string ull_to_literal_string(unsigned long long value)
{
    std::ostringstream ss;
    ss << std::dec << std::setw(0) << value << "ull";
    return ss.str();
}

std::ostream& operator<<(std::ostream& os, const DataStruct& d)
{
    os << "(:key1 " << ull_to_literal_string(d.key1) << ":key2 " << double_to_literal_string(d.key2)
       << ":key3 \"" << d.key3 << "\":)";
    return os;
}

bool compare_by_all_keys(const DataStruct& a, const DataStruct& b)
{
    if (a.key1 != b.key1)
    {
        return a.key1 < b.key1;
    }
    if (a.key2 != b.key2)
    {
        return a.key2 < b.key2;
    }
    return a.key3.length() < b.key3.length();
}

int main()
{
    std::string input_string = "(:key1 123ull:key2 50.0d:key3 \"Rushkevich\":)\n(:key3 \"Stanislav\":key2 10.0D:key1 456ULL:)\n(:key1 123ull:key2 50.0d:key3 \"Rushkevich\":)\n(:key1 123ull:key2 50.0d:key3 \"Vladimirovich\":)\n(:key1 abcull:key2 50.0d:key3 \"Rushkevich\":)";
    std::istringstream iss(input_string);

    std::vector<DataStruct> data;
    std::copy(std::istream_iterator<DataStruct>(iss), std::istream_iterator<DataStruct>(), std::back_inserter(data));

    std::sort(data.begin(), data.end(), compare_by_all_keys);

    std::cout << "Sorted by all keys:\n";
    std::copy(data.begin(), data.end(), std::ostream_iterator<DataStruct>(std::cout, "\n"));

    return 0;
}
