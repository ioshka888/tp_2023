﻿#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <functional>
#include <numeric>
#include <iterator>


namespace nspace
{
    struct Point
    {
        int x_;
        int y_;
    };
    struct Polygon
    {
        std::vector< Point > points;

    };

    struct DelimiterIO
    {
        char exp;
    };

    struct PointIO
    {
        Point& ref;
    };

    class iofmtguard
    {
    public:
        iofmtguard(std::basic_ios< char >& s);
        ~iofmtguard();
    private:
        std::basic_ios< char >& s_;
        char fill_;
        std::streamsize precision_;
        std::basic_ios< char >::fmtflags fmt_;
    };


    std::istream& operator>>(std::istream& in, DelimiterIO&& dest);
    std::istream& operator>>(std::istream& in, PointIO&& dest);
    std::istream& operator>>(std::istream& in, Polygon& dest);
    std::ostream& operator<<(std::ostream& out, const Polygon& dest);
    bool operator==(const Polygon& f1, const Polygon& f2);
    bool isEqual(const Point& v1, const Point& v2);
    double findArea(const Polygon& polygon);
    double areaEven(const std::vector< Polygon >& polygons);
    double areaOdd(const std::vector< Polygon >& polygons);
    double areaMean(const std::vector< Polygon >& polygons);
    double areaVertexes(const std::vector< Polygon >& polygons, size_t numOfVertexes);
    double maxArea(const std::vector< Polygon >& polygons);
    size_t maxVertexes(const std::vector< Polygon >& polygons);
    double minArea(const std::vector< Polygon >& polygons);
    size_t minVertexes(const std::vector< Polygon >& polygons);
    size_t countEven(const std::vector< Polygon >& polygons);
    size_t countOdd(const std::vector< Polygon >& polygons);
    size_t countNumOfVertexes(const std::vector< Polygon >& polygons, size_t numOfVertexes);
    size_t rmecho(std::vector< Polygon >& polygons, Polygon& argument);
    bool inframe(const std::vector<Polygon>& polygons, const Polygon& argument);
    void getResults(std::vector< Polygon >& polygons, std::istream& in);
}

int main()
{
	using namespace nspace;
	std::istringstream iss("   3 (1;8) (4;6) (4;8)  \n\
								4 (-3; 6) (-3; 4) (1; 4) (1; 6) \n\
								4 (1; 1) (4; 1) (4; 4) (1; 4)\n\
                                4 (4; 4) (5; 4) (5; 5) (4; 5) \n\
                                3 (5; 4) (7; 4) (7; 5)\n\
                                3 (5; 4) (7; 4) (7; 5)\n\
                                3 (5; 4) (7; 4) (7; 5)\n\
                                3 (5; 4) (7; 4) (7; 5)\n\
                                3 (5; 4) (7; 4) (7; 5)\n\
                                4 (6; 0) (7; 0) (7; 4) (6; 4) \n\
                                4 not correct line 1) (1; 1) (1; 0) \n\
                                3 (-1; 1) (-1; -4) (3; -4)\n\
                                3 (7; -2) (7; -3) (8; -3) ");
	std::string lineString;
	std::stringstream lineStream;
	std::vector< Polygon > data;

	while (!iss.eof())
	{
        std::getline(iss, lineString);
        lineStream.clear();
        lineStream << lineString;
        Polygon polygon;
        lineStream >> polygon;
        if (lineStream)
        {
            data.push_back(polygon);
        }
        else
        {
            lineStream.clear();
            lineStream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
	}

	std::copy(
		std::begin(data),
		std::end(data),
		std::ostream_iterator< Polygon >(std::cout, "\n")
	);





	std::istringstream commands("  COUNT ODD  \n\
								COUNT EVEN  \n\
								COUNT yy \n\
								COUNT 4 \n\
								AREA ODD\n\
								AREA EVEN\n\
								AREA MEAN\n\
								AREA 4\n\
								AREA 7\n\
                                RMECHO 3 (5; 4) (7; 4) (7; 5)\n\
                                INFRAME 3 (1; 5) (5; 2) (5; 8)\n\
                                INFRAME 3 (1; 5) (5; 2) (5; 9)\n\
                                MAX VERTEXES\n\
                                MIN VERTEXES\n\
                                MAX AREA\n\
                                MIN AREA");

	iofmtguard guard(std::cout);
	std::cout << std::fixed << std::setprecision(1);
	while (!commands.eof())
	{
		try {
			getResults(data, commands);
		}
		catch (const std::invalid_argument& e) {
			std::cerr << e.what() << '\n';
			lineStream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
	}

    std::copy(
        std::begin(data),
        std::end(data),
        std::ostream_iterator< Polygon >(std::cout, "\n")
    );


	return 0;
}

namespace nspace
{
    std::istream& operator>>(std::istream& in, DelimiterIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        char c = '0';
        in >> c;
        if (in && (c != dest.exp))
        {
            in.setstate(std::ios::failbit);
        }
        return in;
    }

    std::istream& operator>>(std::istream& in, PointIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        return in >> DelimiterIO{ '(' } >> dest.ref.x_ >> DelimiterIO{ ';' } >> dest.ref.y_ >> DelimiterIO{ ')' };
    }

    std::istream& operator>>(std::istream& in, Polygon& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }

        Polygon input;

        int numberOfPoints = 0;
        int count = 0;
        in >> numberOfPoints;
        if (numberOfPoints < 1)
        {
            in.setstate(std::ios::failbit);
        }
        for (int i = 0; i < numberOfPoints && in; ++i)
        {
            Point point;
            in >> PointIO{ point };
            if (in)
            {
                count++;
                input.points.push_back(point);
            }
        }

        if (in && count == numberOfPoints)
        {
            dest = input;
        }
        return in;
    }

    std::ostream& operator<<(std::ostream& out, const Polygon& src)
    {
        std::ostream::sentry sentry(out);
        if (!sentry)
        {
            return out;
        }
        iofmtguard fmtguard(out);
        for (size_t i = 0; i < src.points.size(); i++)
        {
            out << "(" << src.points[i].x_ << ";" << src.points[i].y_ << ") ";
        }
        out << "\n";
        return out;
    }

    bool operator==(const Polygon& f1, const Polygon& f2)
    {
        if (f1.points.size() == f2.points.size())
        {
            auto iterator = std::mismatch(f1.points.begin(), f1.points.end(),
                f2.points.begin(), f2.points.end(), isEqual);
            return (iterator.first == f1.points.end());
        }
        else
        {
            return false;
        }
    }

    bool isEqual(const Point& v1, const Point& v2)
    {
        return v1.x_ == v2.x_ && v1.y_ == v2.y_;
    }

    iofmtguard::iofmtguard(std::basic_ios< char >& s) :
        s_(s),
        fill_(s.fill()),
        precision_(s.precision()),
        fmt_(s.flags())
    {}

    iofmtguard::~iofmtguard()
    {
        s_.fill(fill_);
        s_.precision(precision_);
        s_.flags(fmt_);
    }

    double findArea(const Polygon& polygon)
    {
        double area = 0.0;
        size_t n = polygon.points.size();

        for (size_t i = 0; i < n; ++i) {
            const Point& currentVertex = polygon.points[i];
            const Point& nextVertex = polygon.points[(i + 1) % n];

            area += (currentVertex.x_ * nextVertex.y_) - (nextVertex.x_ * currentVertex.y_);
        }


        return std::abs(static_cast<double>(area) / 2.0);
    }

    double areaEven(const std::vector< Polygon >& polygons)
    {
        std::vector<Polygon> figures;
        std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(figures), [](const Polygon& polygon)
            { return polygon.points.size() % 2 == 0; });
        std::vector<double> areas(figures.size());
        std::transform(figures.begin(), figures.end(), areas.begin(), findArea);
        double result = std::accumulate(areas.begin(), areas.end(), 0.0);
        return result;
    }

    double areaOdd(const std::vector< Polygon >& polygons)
    {
        std::vector<Polygon> figures;
        std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(figures), [](const Polygon& polygon)
            { return polygon.points.size() % 2 != 0; });
        std::vector<double> areas(polygons.size());
        std::transform(figures.begin(), figures.end(), areas.begin(), findArea);
        double result = std::accumulate(areas.begin(), areas.end(), 0.0);
        return result;
    }

    double areaMean(const std::vector< Polygon >& polygons)
    {
            std::vector<double> areas(polygons.size());
            std::transform(polygons.begin(), polygons.end(), areas.begin(), findArea);
            double resultArea = std::accumulate(areas.begin(), areas.end(), 0.0);
            double averageArea = resultArea / polygons.size();
            return averageArea;
    }

    double areaVertexes(const std::vector< Polygon >& polygons, size_t numOfVertexes)
    {
        std::vector<Polygon> figures;
        std::copy_if(
            polygons.begin(),
            polygons.end(),
            std::back_inserter(figures),
            [numOfVertexes](const Polygon& polygon) {return polygon.points.size() == numOfVertexes; });
        std::vector<double> areas(figures.size());
        std::transform(figures.begin(), figures.end(), areas.begin(), findArea);
        double result = std::accumulate(areas.begin(), areas.end(), 0.0);
        return result;
    }

    double maxArea(const std::vector< Polygon >& polygons)
    {
        auto it = std::max_element(polygons.begin(), polygons.end(),
            [](const Polygon& p1, const Polygon& p2) {
                return findArea(p1) < findArea(p2);
            });
        return findArea(*it);
    }

    size_t maxVertexes(const std::vector< Polygon >& polygons)
    {
        auto it = std::max_element(polygons.begin(), polygons.end(),
            [](const Polygon& p1, const Polygon& p2) {
                return p1.points.size() < p2.points.size();
            });
        return it->points.size();
    }

    double minArea(const std::vector< Polygon >& polygons)
    {
        auto it = std::min_element(polygons.begin(), polygons.end(),
            [](const Polygon& p1, const Polygon& p2) {
                return findArea(p1) < findArea(p2);
            });
        return findArea(*it);
    }

    size_t minVertexes(const std::vector< Polygon >& polygons)
    {
        auto it = std::min_element(polygons.begin(), polygons.end(),
            [](const Polygon& p1, const Polygon& p2) {
                return p1.points.size() < p2.points.size();
            });
        return it->points.size();
    }


    size_t countEven(const std::vector< Polygon >& polygons)
    {
        return std::count_if(polygons.begin(), polygons.end(), [](const Polygon& polygon)
            { return polygon.points.size() % 2 == 0; });
    }


    size_t countOdd(const std::vector< Polygon >& polygons)
    {
        return std::count_if(polygons.begin(), polygons.end(), [](const Polygon& polygon)
            { return polygon.points.size() % 2 == 1; });
    }

    size_t countNumOfVertexes(const std::vector< Polygon >& polygons, size_t numOfVertexes)
    {
        return std::count_if(polygons.begin(), polygons.end(), [numOfVertexes](const Polygon& polygon)
            { return polygon.points.size() == numOfVertexes; });
    }

    size_t rmecho(std::vector< Polygon >& polygons, Polygon& argument)
    {
        if (polygons.empty())
            return 0;

        int removedCount = 0;

        auto newEnd = std::unique(polygons.begin(), polygons.end(), [&](const Polygon& p1, const Polygon& p2) {
            if (p1 == argument && p2 == argument) {
                ++removedCount;
                return true;
            }
            return false;
            });

        polygons.erase(newEnd, polygons.end());

        return removedCount;
    }

    bool inframe(const std::vector<Polygon>& polygons, const Polygon& argument)
    {
        if (polygons.empty() || argument.points.empty()) {
            return false;
        }

        auto minMaxX = std::minmax_element(polygons.begin(), polygons.end(), [](const Polygon& p1, const Polygon& p2) {
            return std::min_element(p1.points.begin(), p1.points.end(), [](const Point& pt1, const Point& pt2) {
                return pt1.x_ < pt2.x_;
                })->x_ < std::min_element(p2.points.begin(), p2.points.end(), [](const Point& pt1, const Point& pt2) {
                    return pt1.x_ < pt2.x_;
                    })->x_;
            });

        auto minMaxY = std::minmax_element(polygons.begin(), polygons.end(), [](const Polygon& p1, const Polygon& p2) {
            return std::min_element(p1.points.begin(), p1.points.end(), [](const Point& pt1, const Point& pt2) {
                return pt1.y_ < pt2.y_;
                })->y_ < std::min_element(p2.points.begin(), p2.points.end(), [](const Point& pt1, const Point& pt2) {
                    return pt1.y_ < pt2.y_;
                    })->y_;
            });

        int minX = std::min_element((minMaxX.first)->points.begin(), (minMaxX.first)->points.end(), [](const Point& pt1, const Point& pt2) {
            return pt1.x_ < pt2.x_;
            })->x_;

        int maxX = std::max_element((minMaxX.second)->points.begin(), (minMaxX.second)->points.end(), [](const Point& pt1, const Point& pt2) {
            return pt1.x_ < pt2.x_;
            })->x_;

        int minY = std::min_element((minMaxY.first)->points.begin(), (minMaxY.first)->points.end(), [](const Point& pt1, const Point& pt2) {
            return pt1.y_ < pt2.y_;
            })->y_;

        int maxY = std::max_element((minMaxY.second)->points.begin(), (minMaxY.second)->points.end(), [](const Point& pt1, const Point& pt2) {
            return pt1.y_ < pt2.y_;
            })->y_;

        return std::all_of(argument.points.begin(), argument.points.end(), [minX, maxX, minY, maxY](const Point& point) {
            return point.x_ >= minX && point.x_ <= maxX && point.y_ >= minY && point.y_ <= maxY;
            });
    }

    void getResults(std::vector< Polygon >& polygons, std::istream& in)
    {
        std::string INVALID_COMMAND = "INVALID COMMAND";
        std::string command;
        in >> command;
        if (command == "AREA")
        {
            std::string arg;
            in >> arg;
            if (arg == "EVEN")
            {
                std::cout << areaEven(polygons) << std::endl;
            }
            else if (arg == "ODD")
            {
                std::cout << areaOdd(polygons) << std::endl;
            }
            else if (arg == "MEAN")
            {
                if (polygons.size() == 0)
                {
                    throw std::invalid_argument(INVALID_COMMAND);
                }
                std::cout << areaMean(polygons) << std::endl;
            }
            else
            {
                size_t vertexes = 0;
                try
                {
                    vertexes = std::stoull(arg);
                }
                catch (...)
                {
                    throw std::invalid_argument(INVALID_COMMAND);
                }
                std::cout << areaVertexes(polygons, vertexes) << std::endl;
            }
        }
        else if (command == "MAX")
        {
            if (polygons.size() == 0)
            {
                throw std::invalid_argument(INVALID_COMMAND);
            }
            std::string arg;
            in >> arg;
            if (arg == "AREA")
            {
                std::cout << maxArea(polygons) << std::endl;
            }
            else if (arg == "VERTEXES")
            {
                std::cout << maxVertexes(polygons) << std::endl;
            }
            else
            {
                throw std::invalid_argument(INVALID_COMMAND);
            }
        }
        else if (command == "MIN")
        {
            if (polygons.size() == 0)
            {
                throw std::invalid_argument(INVALID_COMMAND);
            }
            std::string arg;
            in >> arg;
            if (arg == "AREA")
            {
                std::cout << minArea(polygons) << std::endl;
            }
            else if (arg == "VERTEXES")
            {
                std::cout << minVertexes(polygons) << std::endl;
            }
            else
            {
                throw std::invalid_argument(INVALID_COMMAND);
            }
        }
        else if (command == "COUNT")
        {
            std::string arg;
            in >> arg;
            if (arg == "EVEN")
            {
                std::cout << countEven(polygons) << std::endl;
            }
            else if (arg == "ODD")
            {
                std::cout << countOdd(polygons) << std::endl;
            }
            else
            {
                size_t vertexes = 0;
                try
                {
                    vertexes = std::stoull(arg);
                }
                catch (...)
                {
                    throw std::invalid_argument(INVALID_COMMAND);
                }
                std::cout << countNumOfVertexes(polygons, vertexes) << std::endl;
            }
        }
        else if (command == "RMECHO")
        {
            Polygon arg;
            in >> arg;
            if (!in)
            {
                throw std::invalid_argument(INVALID_COMMAND);
            }
            std::cout << rmecho(polygons, arg) << std::endl;
        }
        else if (command == "INFRAME")
        {
            if (polygons.size() == 0)
            {
                throw std::invalid_argument(INVALID_COMMAND);
            }
            Polygon arg;
            in >> arg;
            if (!in)
            {
                throw std::invalid_argument(INVALID_COMMAND);
            }
            std::cout << (inframe(polygons, arg) ? "<TRUE>" : "<FALSE>") << std::endl;
        }
        else
        {
            in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }
}