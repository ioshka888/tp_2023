#pragma once
#ifndef HEADER_HASH
#define HEADER_HASH

#include <string>
#include <iostream>
#include <unordered_map>
#include <vector>



const std::size_t TABLE_SIZE = 500;

struct Node
{
    std::string key;
    std::string value;
    Node* next;
};

class EnglishRussianDictionary
{
public:
    EnglishRussianDictionary() = default;

    void insert(const std::string& key, const std::string& value) // ������� ��� ������� �����
    {
        // ��������� ���� ����-�������� � ���-�������
        dictionary[key] = value;
    }

    bool contains(const std::string& key) const // ������� ������
    {
        // ��������� ������� ����� � ���-�������
        return dictionary.count(key) > 0;
    }

    std::string& operator[](const std::string& key)
        // �������� ������� � �������� ������� �� �����. �� ��������� ���� � �������� ��������� � ���������� ������ �� ��������, ��������������� ����� �����
    {
        // ���������� ��������, ��������������� �����
        return dictionary[key];
    }

    void erase(const std::string& key)
    {
        // ������� ���� ����-�������� �� ���-�������
        dictionary.erase(key);
    }

private:
    std::unordered_map<std::string, std::string> dictionary;
};
#endif // !HEADER_HASH
//class Dictionary
//{
//public:
//    Dictionary() : table_size(500), hash_table(std::vector<Dictionary>(table_size)) {}
//
//    void insert(const std::string& key, const std::string& value)
//    {
//        int index = hash_function(key);
//        hash_table[index].insert(key, value);
//    }
//
//    bool contains(const std::string& key) const
//    {
//        int index = hash_function(key);
//        return hash_table[index].contains(key);
//    }
//
//    std::string& operator[](const std::string& key)
//    {
//        int index = hash_function(key);
//        return hash_table[index][key];
//    }
//
//    void erase(const std::string& key)
//    {
//        int index = hash_function(key);
//        hash_table[index].erase(key);
//    }
//
//private:
//    int table_size;
//    std::vector<Dictionary> hash_table;
//
//    int hash_function(const std::string& key) const
//    {
//        int hash = 0;
//        for (char c : key)
//        {
//            hash += c;
//        }
//        return hash % table_size;
//    }
//};




//struct Node 
//{
//    std::string key;
//    std::string value;
//   /* std::unique_ptr<Node> left;
//    std::unique_ptr<Node> right;*/
//    Node* next;
//};
//
//class EnglishRussianDictionary
//{
//public:
//    EnglishRussianDictionary() = default;
//
//    void insert(const std::string& key, const std::string& value) // ������� ��� ������� �����
//    {
//        if (!root) 
//        {
//            root = std::make_unique<Node>(Node{ key, value, nullptr, nullptr });
//            return;
//        }
//
//        Node* current = root.get();
//        while (current) 
//        {
//            if (key < current->key) 
//            {
//                if (!current->left) 
//                {
//                    current->left = std::make_unique<Node>(Node{ key, value, nullptr, nullptr });
//                    return;
//                }
//                current = current->left.get();
//            }
//            else if (key > current->key) 
//            {
//                if (!current->right) 
//                {
//                    current->right = std::make_unique<Node>(Node{ key, value, nullptr, nullptr });
//                    return;
//                }
//                current = current->right.get();
//            }
//            else 
//            {
//                current->value = value;
//                return;
//            }
//        }
//    }
//
//    bool contains(const std::string& key) const // ������� ������
//    {
//        Node* current = root.get();
//        while (current) 
//        {
//            if (key < current->key) 
//            {
//                current = current->left.get();
//            }
//            else if (key > current->key) 
//            {
//                current = current->right.get();
//            }
//            else 
//            {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    std::string& operator[](const std::string& key)
//    // �������� ������� � �������� ������� �� �����. �� ��������� ���� � �������� ��������� � ���������� ������ �� ��������, ��������������� ����� �����
//    {
//        Node* current = root.get();
//        while (current) 
//        {
//            if (key < current->key) 
//            {
//                if (!current->left) 
//                {
//                    current->left = std::make_unique<Node>(Node{ key, "", nullptr, nullptr });
//                }
//                current = current->left.get();
//            }
//            else if (key > current->key) 
//            {
//                if (!current->right) 
//                {
//                    current->right = std::make_unique<Node>(Node{ key, "", nullptr, nullptr });
//                }
//                current = current->right.get();
//            }
//            else 
//            {
//                return current->value;
//            }
//        }
//    }
//
//    void erase(const std::string& key) 
//    {
//        root = erase(root, key);
//    }
//
//private:
//    std::unique_ptr<Node> root;
//
//    std::unique_ptr<Node> erase(std::unique_ptr<Node>& node, const std::string& key) {
//        if (!node) 
//        {
//            return nullptr;
//        }
//
//        if (key < node->key) 
//        {
//            node->left = erase(node->left, key);
//            return nullptr;
//        }
//
//        if (key > node->key) 
//        {
//            node->right = erase(node->right, key);
//            return nullptr;
//        }
//
//        if (!node->left && !node->right) 
//        {
//            return nullptr;
//        }
//
//        if (!node->left) 
//        {
//            return std::move(node->right);
//        }
//
//        if (!node->right) 
//        {
//            return std::move(node->left);
//        }
//
//        Node* min_right = node->right.get();
//        while (min_right->left) 
//        {
//            min_right = min_right->left.get();
//        }
//
//        node->key = std::move(min_right->key);
//        node->value = std::move(min_right->value);
//        node->right = erase(node->right, node->key);
//
//        return nullptr;
//    }
//};
//
