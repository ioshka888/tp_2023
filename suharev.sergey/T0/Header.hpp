//Suharev. Пирамидальная сортировка (Heapsort, по не убыванию)
#ifndef HEADER
#define HEADER
#include <iostream>


// Функция для просеивания элементов в куче
void heapify(int arr[], int n, int i)
{

    int largest = i;       // Инициализируем наибольший элемент как корень
    int left = 2 * i + 1;  // левый = 2i + 1
    int right = 2 * i + 2; // правый = 2*i + 2

    // Если левый потомок больше корня
    if (left < n && arr[left] > arr[largest])
    {
        largest = left;
    }
    // Если правый потомок больше наибольшего на данный момент
    if (right < n && arr[right] > arr[largest])
    {
        largest = right;
    }
    // Если наибольший элемент не является корнем
    if (largest != i)
    {
        std::swap(arr[i], arr[largest]);

        // Применяем просеивание для поддерева
        heapify(arr, n, largest);
    }
}

// Основная функция сортировки
void heapsort(int arr[], int n)
{
    // Построение кучи
    for (int i = n / 2 - 1; i >= 0; i--)
    {
        heapify(arr, n, i);
    }
    
    // Извлечение элементов из кучи по одному
    for (int i = n - 1; i >= 0; i--)
    {
        // Перемещаем текущий корень в конец
        std::swap(arr[0], arr[i]);

        // Вызываем heapify на уменьшенной куче
        heapify(arr, i, 0);
    }
}

// Функция для проверки упорядоченности последовательности
bool is_sorted(int arr[], int n)
{
    for (int i = 1; i < n; ++i)
    {
        if (arr[i - 1] > arr[i])
        {
            return false;
        }
    }
    return true;
}
//Fix 06
#endif