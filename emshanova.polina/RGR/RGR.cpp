﻿#include <iostream>
#include <fstream>
#include <sstream>
#include "EngRusDictionary.h"
#include <unordered_map>
#include <filesystem>
#include <locale>
#include <algorithm>
#include <cctype>

bool isEnglishWord(const std::string& word);
void strToLower(std::string& word);
void testInterface();

int main()
{
	setlocale(LC_ALL, "RUSSIAN");

	testInterface();

	return 0;
}

bool isEnglishWord(const std::string& word)
{
	for (char c : word)
	{
		if (!std::isalpha(c))
		{
			return false;
		}
	}
	return true;
}

void strToLower(std::string& word)
{
	for (char& c : word)
	{
		c = std::tolower(c);
	}
}

void testInterface()
{
	EngRusDictionary myDictionary;
	std::string filename = "";
	std::string word = "";
	std::string translation = "";
	std::string command = "";

	std::cout << "\nтеперь вы можете взаимодействовать со словарем\n\n";
	std::cout << "введите <INSERT_FROM_FILE filename> чтобы считать слова и переводы из файла\n";
	std::cout << "введите <INSERT word translation> чтобы добавить в словарь новое слово и перевод\n";
	std::cout << "введите <DELETE word> чтобы удалить из словаря слово и его перевод(ы)\n";
	std::cout << "введите <SEARCH word> чтобы найти в словаре перевод(ы) слова\n";
	std::cout << "введите <PRINT> чтобы вывести весь словарь на экран\n\n";
	std::cout << "введите <EXIT> чтобы выйти из программы\n\n";

	std::istringstream commands(" 	INSERT MY МОЙ\n\
									INSERT MY МОЯ\n\
									INSERT MY МОИ\n\
									INSERT MY МОЁ\n\
									INSERT YOUR ТВОЙ\n\
									INSERT YOUR ТВОИ\n\
									INSERT SKY НЕБО\n\
									INSERT SUMMER ЛЕТО\n\
									SEARCH MY\n\
									SEARCH SUMMER\n\
									PRINT\n\
									DELETE SUMMER\n\
									SEARCH SUMMER\n\
									PRINT\n\
									INSERT 6546 ТЫ\n\
									INSERT ТЫ ТЫ\n\
									DELETE 54738\n\
									SEARCH 4374\n\
									HHHFHGGF HJD\n\
									INSERT_FROM_FILE file.txt\n\
									PRINT\n\
									SEARCH MOTHER\n\
									SEARCH WWW\n\
									EXIT");


	while (true)
	{
		commands >> command;
		if (command == "INSERT_FROM_FILE")
		{
			commands >> filename;
			std::ifstream inputFile(filename);
			if (!inputFile.is_open())
			{
				std::cerr << "файл не удалось открыть\n";
				inputFile.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				continue;
			}
			while (!inputFile.eof())
			{
				inputFile >> word;
				if (!isEnglishWord(word))
				{
					inputFile.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
					continue;
				}
				inputFile >> translation;
				strToLower(word);
				strToLower(translation);
				myDictionary.insertWord(word, translation);
			}
			std::cout << "словарь успешно считан \n\n";
		}
		else if (command == "INSERT")
		{
			commands >> word;
			if (!isEnglishWord(word))
			{
				std::cout << "слово не является английским словом, введите команду заново\n";
				commands.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				continue;
			}
			commands >> translation;
			strToLower(word);
			strToLower(translation);
			if (myDictionary.insertWord(word, translation))
			{
				std::cout << word << " - " << translation << " было добавлено в словарь\n";
			}
			else
			{
				std::cout << word << " - " << translation << " уже есть в словаре\n";
			}
		}

		else if (command == "DELETE")
		{
			commands >> word;
			if (!isEnglishWord(word))
			{
				std::cout << "слово, которое вы хотите удалить, не является английским словом, в словаре его точно нет, введите команду заново\n";
				commands.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				continue;
			}
			strToLower(word);
			if (myDictionary.deleteWord(word))
			{
				std::cout << word << " было удалено\n";
			}
			else
			{
				std::cout << word << " не было в словаре, удалять нечего\n";
			}
		}

		else if (command == "SEARCH")
		{
			commands >> word;
			if (!isEnglishWord(word))
			{
				std::cout << "слово, которое вы ищете, не является английским словом, в словаре его точно нет, введите команду заново\n";
				commands.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				continue;
			}
			strToLower(word);
			myDictionary.searchWord(word);
		}

		else if (command == "EXIT")
		{
			std::cout << "до свидания!\n";
			break;
		}

		else if (command == "PRINT")
		{
			myDictionary.printDictionary();
		}

		else
		{
			std::cout << "введите команду еще раз, без ошибок\n";
		}

		commands.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}
}
