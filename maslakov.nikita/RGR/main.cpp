﻿#include <iostream>
#include <string>
#include "dictionary.hpp"

int main() {

    setlocale(LC_ALL, "ru");

    Dictionary dictionary;

    dictionary.insert("hello", "привет");
    dictionary.insert("world", "мир");
    dictionary.insert("open", "открыть");
    dictionary.insert("close", "закрыть");

    std::string translation = dictionary.search("hello");
    std::cout << "Translation of 'hello': " << translation << std::endl;

    translation = dictionary.search("open");
    std::cout << "Translation of 'open': " << translation << std::endl;

    translation = dictionary.search("test");
    std::cout << "Translation of 'test': " << translation << std::endl;

    bool removed = dictionary.remove("world");
    if (removed) {
        std::cout << "Word 'world' removed successfully!" << std::endl;
    }
    else {
        std::cout << "Word 'world' not found in the dictionary." << std::endl;
    }

    removed = dictionary.remove("test");
    if (removed) {
        std::cout << "Word 'test' removed successfully!" << std::endl;
    }
    else {
        std::cout << "Word 'test' not found in the dictionary." << std::endl;
    }

    std::cout << "Dictionary:" << std::endl;
    dictionary.print();

    return 0;
}
