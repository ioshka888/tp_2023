#include <iostream>
#include <string>  
#include <vector>  
#include <algorithm>  
#include <iomanip>  
#include <sstream>  
#include <iterator>  
#include <complex> 

struct DataStruct
{
    std::complex<double> key1;
    std::pair<long long, unsigned long long> key2;
    std::string key3;
};

std::istream& operator>>(std::istream& iss, DataStruct& data)
{
    char c;
    while (iss >> c)
    {
        if (c == ':')
        {
            std::string key;
            iss >> key;
            if (key == "key1")
            {
                std::string value;
                iss >> c; // #
                iss >> c; // c
                iss >> c; // (  
                double re, im;
                iss >> re >> im;  // 1.0 -1.0  
                iss >> c; // )
                data.key1 = { re, im };
            }
            else if (key == "key2")
            {
                iss >> c;  // (  
                iss >> c;  // :  
                std::string name;
                iss >> name;  // N  
                long long n;
                iss >> n;
                iss >> c;  // :  
                iss >> c;  // D  
                unsigned long long d;
                iss >> d;
                iss >> c;  // ) 
                data.key2 = { n, d };
            }
            else if (key == "key3")
            {
                std::string value;
                if (iss >> c && c == '"' && getline(iss, value, '"'))
                {
                    data.key3 = value;
                }
                else
                {
                    return iss;
                }
            }
            else
            {
                break;
            }
        }
        else if (c == ')')
            break;
    }
    return iss;
}

std::string double_to_literal_string(double value)
{
    std::ostringstream ss;
    ss << std::fixed << std::setprecision(1) << value;
    return ss.str();
}

std::string pair_to_string(const std::pair<long long, unsigned long long>& p)
{
    std::ostringstream ss;
    ss << "(:N " << p.first << ":D " << p.second << ":)";
    return ss.str();
}

std::string complex_to_string(const std::complex<double>& c)
{
    std::ostringstream ss;
    ss << "#c(" << c.real() << ' ' << c.imag() << ")";
    return ss.str();
}

std::ostream& operator<<(std::ostream& os, const DataStruct& d)
{
    os << ":key1 " << complex_to_string(d.key1) << ":key2 " << pair_to_string(d.key2)
        << ":key3 \"" << d.key3 << "\"";
    return os;
}

bool compare_by_all_keys(const DataStruct& a, const DataStruct& b)
{
    double amod = std::abs(a.key1);
    double bmod = std::abs(b.key1);
    if (amod != bmod)
        return amod < bmod;
    if (a.key2.first != b.key2.first)
        return a.key2.first < b.key2.first;
    if (a.key2.second != b.key2.second)
        return a.key2.second < b.key2.second;
    return a.key3.length() < b.key3.length();
}

int main()
{
    std::string input_string = "(:key1 #c(1.0 -1.0):key2 (:N -1:D 5:):key3 \"data\":)\n"
        "(:key2 (:N -1:D 5:):key3 \"with : inside\":key1 #c(2.0 -3.0):)\n";
    std::istringstream iss(input_string);

    std::vector<DataStruct> data;
    std::copy(std::istream_iterator<DataStruct>(iss), std::istream_iterator<DataStruct>(),
        std::back_inserter(data));

    std::sort(data.begin(), data.end(), compare_by_all_keys);

    std::cout << "Sorted by all keys:\n";
    std::copy(data.begin(), data.end(), std::ostream_iterator<DataStruct>(std::cout, "\n"));

    return 0;
}
